import { ComponentStory } from "@storybook/react";
import React from "react";
import { Radio } from "./Radio";
import "../../styles/index.scss";

export default {
  title: "Kit/Radio",
  component: Radio,
};

const Template: ComponentStory<typeof Radio> = (args) => <Radio {...args} />;

export const Checked = Template.bind({});
Checked.args = {
  text: "Radio",
  checked: true,
  name: "radio",
  value: "1",
};

export const NotChecked = Template.bind({});
NotChecked.args = {
  text: "Radio",
  checked: false,
  name: "radio",
  value: "1",
};
