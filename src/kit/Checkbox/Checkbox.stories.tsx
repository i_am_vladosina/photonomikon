import { ComponentStory } from "@storybook/react";
import React from "react";
import { Checkbox } from "./Checkbox";

import "../../styles/index.scss";

export default {
  title: "Kit/Checkbox",
  component: Checkbox,
};

const Template: ComponentStory<typeof Checkbox> = (args) => <Checkbox {...args} />;

export const Checked = Template.bind({});
Checked.args = {
  text: "Checked Checkbox",
  checked: true,
  name: "checkbox",
  value: "1",
};

export const NotChecked = Template.bind({});
NotChecked.args = {
  text: "Not Checked Checkbox",
  checked: false,
  name: "checkbox",
  value: "1",
};
