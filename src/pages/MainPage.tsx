import React, { useState } from "react";
import { Checkbox } from "src/kit/Checkbox/Checkbox";
import { Radio } from "src/kit/Radio/Radio";

export function MainPage() {
  const [radioActive, setRadioActive] = useState<"1" | "2">("1");
  const [checkboxActive, setCheckboxActive] = useState<"1" | "2">("1");

  return (
    <div>
      <p>Photonomikon Main Page</p>
      <div>
        <Radio
          text="Active Radio"
          name="radio"
          value="1"
          checked={radioActive === "1"}
          onChange={(_, v) => {
            setRadioActive("1");
          }}
        />
        <Radio
          text="Inactive Radio"
          name="radio"
          value="2"
          checked={radioActive === "2"}
          onChange={(_, v) => {
            setRadioActive("2");
          }}
        />
      </div>

      <div>
        <Checkbox
          text="1 Checkbox"
          name="checkbox"
          value="1"
          checked={checkboxActive === "1"}
          onChange={(_, v) => {
            setCheckboxActive("1");
          }}
        />
        <Checkbox
          text="2 Checkbox"
          name="checkbox"
          value="2"
          checked={checkboxActive === "2"}
          onChange={(_, v) => {
            setCheckboxActive("2");
          }}
        />
      </div>
    </div>
  );
}
