import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { MainPage } from "./pages/MainPage";
import { ROUTES } from "./routes";

export function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={ROUTES.Main} element={<MainPage />} />
      </Routes>
    </BrowserRouter>
  );
}
